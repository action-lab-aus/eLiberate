# eLiberate

Since its inception the Internet has been touted as a medium with revolutionary potential for democratic communication. Although other media including television and radio have not lived up to their democratic potential, it is too early to dismiss the Internet as being predominantly a tool for the powerful. Certainly civil society has been extraordinarily creative in using the Internet for positive social change. Our goal is to create an engine for deliberation using the power of the Internet.

## Configuration

Set the following environment vars in docker-compose.yml.

```
MAIL_URL: ""
MAIL_FROM: ""
ADMINS: "tom"
```

## Development (docker)

<!-- Build and deploy using `docker compose build` and `docker compose deploy`. -->

Use `docker compose -f docker-compose-dev.yml up` to spin up a local container which runs meteor in dev mode and maps the local source directories live.

## Deployment

Set env variables in docker-compose.yml and run:

Use `docker compose up -d`

## Local Dev

Install https://www.meteor.com/install
Run `meteor run --port $PORT`
