#!/bin/bash

set -o errexit

printf "\n[-] Running Meteor dev env...\n"

cd $APP_SOURCE_FOLDER

meteor npm install

meteor run --port 3000